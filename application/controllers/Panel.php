<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 2/08/2018
 * Time: 7:51 PM
 */
class Panel extends CI_Controller{


	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->idUsuario)){
			redirect("https://www.queltalar.online/woltk/");
		}
	}

	public function index(){
		if(isset($this->session->idUsuario)){
			$this->We->obtenerIdAcc();
			$o = $this->C->online();
			$data = array(
				"usuario" => $this->session->username,
				"correo" => $this->session->email,
				"nombreCompleto" => $this->session->nombre,
				"rango" => $this->session->rango,
				"perfilFoto" => (isset($this->session->img)) ? $this->session->img : 'illidan-portrait.jpg',
				"hordaOnline" => $o->onlineH,
				"alizOnline" => $o->onlineA,
				"pandaNuestral" => $o->onlineN,
				"onlinePlayers" => $o->onlineT,
				"discord" => $this->We->getDiscordInfo(),
				"onlineWeb" => $this->We->online()
			);
			$this->load->view("panel/index",$data);
		}else{
			redirect(base_url());
		}
	}

	public function bugtracker(){
		echo json_encode($this->We->get_Bugtracker());
		exit();
	}

	public function salir(){
		session_destroy();
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('idUsuario');
		$this->session->unset_userdata('imagen');
		$this->session->sess_destroy();
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
		redirect(base_url());
	}

	public function guardarReporte(){
		if(isset($_POST["titulo"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"description" => ($this->input->post("html")),
					"title" => $this->input->post("titulo"),
					"author" => $this->session->idAcc,
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->We->registrarWeb("bugtracker",$data));
			}
		}else{
			echo json_encode(false);
		}
	}
	function guardarComentarioReporte(){
		if(isset($_POST["idB"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"idBugtracker" => ($this->input->post("idB")),
					"idAuthor" => $this->session->idAcc,
					"comentario" => $this->input->post("comentario"),
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->We->registrarWeb("comment_bugtracker",$data));
			}
		}else{
			echo json_encode(false);
		}
	}

	function buscarComentariosReportes(){
		if(isset($_GET["id"])){
			echo json_encode($this->We->comentariosBugtracker($this->input->get("id")));
		}
	}


	public function eliminarReporte(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->We->updateWeb("bugtracker",array("close"=>1),array("id"=>$this->input->post("id"))));
				}else{
					json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarPostulacion(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->We->updateWeb("postulacion",array("close"=>1),array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarComentarioReporte(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->We->deleteWeb("comment_bugtracker",array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}
			}
		}else{
			echo json_encode(false);
		}
	}

	public function eliminarComentarioPostulacion(){
		if(isset($_POST["id"])){
			if(isset($this->session->idUsuario)){
				if($this->session->idAcc == $this->input->post("idAcc")){
					echo json_encode($this->We->deleteWeb("comment_postulacion",array("id"=>$this->input->post("id"))));
				}else{
					echo json_encode(false);
				}

			}
		}else{
			echo json_encode(false);
		}
	}

	public function uploads(){
		// Allowed extentions.
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		if(!isset($_FILES["file"]["name"])){
			exit(0);
		}
		// Get filename.
		$temp = explode(".", $_FILES["file"]["name"]);

		// Get extension.
		$extension = end($temp);

		// An image check is being done in the editor but it is best to
		// check that again on the server side.
		// Do not use $_FILES["file"]["type"] as it can be easily forged.
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		//echo $_FILES["file"]["tmp_name"];

		$mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

		if ((($mime == "image/gif")
				|| ($mime == "image/jpeg")
				|| ($mime == "image/pjpeg")
				|| ($mime == "image/x-png")
				|| ($mime == "image/png"))
			&& in_array($extension, $allowedExts)) {
			// Generate new random name.
			$name = sha1(microtime()) . "." . $extension;

			// Save file in the uploads folder.
			if(isset($_POST["perfil"])){
				move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . "/assets/panel/perfil/" . $name);
				if($this->We->updateWeb("account",array("imagen"=>$name),array("id"=>$this->session->idAcc))){
					unlink("assets/panel/perfil/".$this->session->img);
					$this->session->img = $name;
					echo json_encode(true);
					exit(0);
				}
			}else{
				move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . "/assets/panel/uploads/" . $name);
			}
			// Generate response.
			$response = new StdClass;
			$response->link = "/uploads/" . $name;
			echo stripslashes(json_encode($response));
		}
	}

	public function jsonImg(){
		$directory="assets/panel/uploads/";
		$dirint = dir($directory);
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$data = array();
		while (($archivo = $dirint->read()) !== false)
		{
			$temp = explode(".",$archivo);
			// Get extension.
			$extension = end($temp);
			if(in_array($extension, $allowedExts)){
				array_push($data,array("url"=>"/".$directory."/".$archivo,"thumb"=>"/".$directory."/".$archivo));
			}else{
				continue;
			}
		}
		$dirint->close();
		echo json_encode(($data));
	}

	public function guardarPostulacion(){
		if(isset($_POST["titulo"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"description" => ($this->input->post("html")),
					"title" => $this->input->post("titulo"),
					"author" => $this->session->idAcc,
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->We->registrarWeb("postulacion",$data));
			}
		}else{
			echo json_encode(false);
		}
	}


	function guardarComentarioPostulacion(){
		if(isset($_POST["idB"])){
			if(isset($this->session->idUsuario)){
				$fecha = new DateTime();
				$fecha->getTimestamp();
				$data = array(
					"idPostulacion" => ($this->input->post("idB")),
					"idAuthor" => $this->session->idAcc,
					"comentario" => $this->input->post("comentario"),
					"date" => $fecha->getTimestamp()
				);
				echo json_encode($this->We->registrarWeb("comment_postulacion",$data));
			}
		}else{
			echo json_encode(false);
		}
	}


	public function postulacion(){
		echo json_encode($this->We->get_Postulacion());
		exit();
	}

	function buscarComentarioPostulacion(){
		if(isset($_GET["id"])){
			echo json_encode($this->We->comentariosPostulacion($this->input->get("id")));
		}
	}

	public function actualizarInformacionPersonal(){
		if(isset($_POST["idAcc"])){
			if($this->session->idAcc == $this->input->post("idAcc")){
				$password = $this->input->post("password");
				if($password != ""){
					if($this->We->updateWeb("account",
						array(
							"nombre" => $this->input->post("nombre"),
							"facebook" => $this->input->post("facebook"),
							"twitter" => $this->input->post("twitter"),
							"telefono" => $this->input->post("telefono"),
							"password" => $password
						),
						array("id"=>$this->session->idAcc))){

						$sha_pass_hash1 = strtoupper(($this->We->encriptacion($this->session->username,$password)));
						$sha_pass_hash2 = strtoupper(($this->We->encriptacion($this->session->email,$password)));
						$this->A->updateAuth("account",array("sha_pass_hash"=>$sha_pass_hash1,"sessionkey"=>""),array("id"=>$this->session->idUsuario));
						$this->A->updateAuth("battlenet_accounts",array("sha_pass_hash"=>$sha_pass_hash2),array("id"=>$this->session->idUsuario));
						echo json_encode(true);
						exit(0);
					}
				}else{
					if($this->We->updateWeb("account",
						array(
							"nombre" => $this->input->post("nombre"),
							"facebook" => $this->input->post("facebook"),
							"twitter" => $this->input->post("twitter"),
							"telefono" => $this->input->post("telefono")

						),
						array("id"=>$this->session->idAcc))){
						echo json_encode(true);
						exit(0);
					}
				}
			}
		}else{
			echo json_encode(false);
		}
	}


	public function getAccount(){
		if($this->session->rango>=6){
			echo json_encode($this->A->getAccount($this->input->post("cuentaGM")));
		}
	}

	public function getRangos(){
		echo json_encode($this->A->getRangos());
	}
	public function setRango(){
		if($this->session->rango>=6){
			$data = [
				"id" => $this->input->post("id"),
				"gmlevel" => $this->input->post("gmlevel")
			];
			echo json_encode($this->A->setRango($data));	
		}else{
			echo json_encode(false);
		}
	}
}
