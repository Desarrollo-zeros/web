<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wow extends CI_Controller {

	public $variablesHead = array();

	/**
	 * Stormmane constructor.
	 */


	public function index()
	{
		if(isset($this->session->idUsuario)){
			redirect("https://www.queltalar.online/QueltalarPanel/");
		}else{
			redirect("https://www.queltalar.online/woltk/");
		}
	}


	public function login($sha_pass_hash){
		$sha_pass_hash = base64_decode($sha_pass_hash);
		if(($this->We->login($sha_pass_hash))){
			redirect("https://www.queltalar.online/QueltalarPanel/panel");
		}else{
			redirect("https://www.queltalar.online/woltk/");
		}
	}


	public function iniciarSesion(){
		if(isset($_POST["usuario"]) && isset($_POST["password"])){
			$user = strtoupper($this->input->post("usuario"));
			$pass = strtoupper($this->input->post("password"));
			$username = ((bool)filter_var($user,FILTER_VALIDATE_EMAIL))
				      ?  strtoupper($this->A->get_usuario($user))
				      :	 strtoupper($user);
			echo ($this->We->iniciar_sesion($username,$pass)) ? json_encode(true) : json_encode(false);
			exit();
		}else{
			redirect(base_url(""));
		}
	}

	public function registrarUsuario(){
		if(isset($_POST)){
			$user = strtoupper($this->input->post("usuario"));
			$pass = strtoupper($this->input->post("password"));
			$email = strtoupper($this->input->post("email"));

			$sha_pass_hash = strtoupper(($this->We->encriptacion($user,$pass)));

			if(isset($_POST)){
				unset($data);
				$data = array(
					'username' => $user,
					'sha_pass_hash' => $sha_pass_hash,
					'email' => $email,
					'expansion' => '7',
					'battlenet_index' => '1',
				);
				if($this->A->registrarAuth("account",$data)){
					$sha_pass_hash = strtoupper(($this->We->encriptacion($email,$pass)));
					$id = $this->A->getIdAccount($user);
					$data = array(
						'id' => $id,
						'sha_pass_hash' => $sha_pass_hash,
						'email' => $email,
					);
					if($this->A->registrarAuth("battlenet_accounts",$data)){
						unset($data);
						$img = ['anduin-portrait','athissa-portrait','dargrul-portrait','genn-portrait','guldan-portrait','illidan-portrait','khadgar-portrait','maiev-portrait','sylvanas-portrait','xavius-portrait'];
						$getImagen =  $img[mt_rand(0,9)].".jpg";
						$data = array("idAccount"=>$id,'username' => $user,'email' => $email,"password" => $pass, "imagen" =>$getImagen);
						echo json_encode($this->We->registrarWeb("account",$data));
						exit();
					}
				}
			}
		}else{
			redirect(base_url(""));
		}

	}

	function validarExits(){
		if(isset($_POST)){
			if(isset($_GET)){
				echo json_encode($this->A->validarExits($this->input->get("id"),strtoupper($this->input->post("data"))));
				exit();
			}else{
				redirect(base_url(""));
			}
		}else{
			redirect(base_url(""));
		}
	}


	function mensaje(){
		if(isset($_GET)){
			$this->load->view("pagina/mensajes");
		}
	}

	function recuperacion(){
		if(isset($_POST)){
			$correo = strtoupper($this->input->post("email"));
			$d = $this->We->obtenerDatosRecuperacion($correo);
			if(isset($d)){
				echo json_encode($this->We->recuperacionCuenta($d->username,$d->password,$d->email));
			}else{
				echo json_encode(false);
			}
		}
	}

	function header_sent($header) {
		$headers = headers_list();
		$header = trim($header,': ');
		$result = false;

		foreach ($headers as $hdr) {
			if (stripos($hdr, $header) !== false) {
				$result = true;
			}
		}

		return $result;
	}

}
