<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: carlo
 * Date: 13/12/2018
 * Time: 2:36 PM
 */


use Coolpraz\PhpBlade\PhpBlade;
class MY_Controller extends CI_Controller
{
	protected $view = APPPATH . 'views';
	protected $cache = APPPATH . 'cache';
	protected $blade;

	public function __construct(){
		parent::__construct();
		$this->blade = new PhpBlade($this->view, $this->cache);
	}


	protected function view($name, $body = [])
	{
		echo isset($body)?
			$this->blade->view()->make("$name", $body)->render() :
			$this->blade->view($name)->render();
	}
}
