<?php
/**
 * Created by PhpStorm.
 * User: zeros
 * Date: 2/08/2018
 * Time: 11:27 PM
 */
class Web extends CI_Model{

	public function __construct()
	{
		parent::__construct();
	}
	public function registrarWeb($tabla, $data = array()){
		return $this->db->insert($tabla, $data);
	}

	public function validarExistUsuario($u){
		return ($this->db->query("select id from account where username = '$u'")->num_rows() > 0) ? true : false;
	}

	public function updateWeb($tabla,$data=array(),$where = array()){
		return $this->db->update($tabla,$data,$where);
	}
	public function deleteWeb($tabla,$data=array()){
		return $this->db->delete($tabla,$data);
	}

	public function login($sha_pass_hash){
		$pass = $sha_pass_hash;
		$query = $this->A->get_UserDate1($pass);
		if($query->num_rows() > 0){
			$this->session->idUsuario = $query->row("id");
			$this->session->username = $query->row("username");
			$this->session->email = $query->row("email");
			if(!$this->validarExistUsuario($query->row("username"))){
				$img = ['anduin-portrait','athissa-portrait','dargrul-portrait','genn-portrait','guldan-portrait','illidan-portrait','khadgar-portrait','maiev-portrait','sylvanas-portrait','xavius-portrait'];
				$getImagen =  $img[mt_rand(0,9)].".jpg";
				$data = array(
					"idAccount" => $this->session->idUsuario,
					"username" => $query->row("username"),
					"email" => $query->row("email"),
					"password" => $pass,
					"imagen" =>$getImagen
				);
				$this->session->img = $getImagen;
				if($this->registrarWeb("account",$data)){
					return true;
				}
			}else{
				$u = $query->row("username");
				$data =  $this->db->query("select imagen,nombre, facebook, twitter, rango from account where username = '{$u}'")->row();
				$this->session->img = $data->imagen;
				$this->session->nombre = $data->nombre;
				$this->session->facebook = $data->facebook;
				$this->session->rango = $data->rango;
				$this->session->twitter = $data->twitter;
			}
			return true;
		}else{
			return false;
		}
	}

	public function iniciar_sesion($u,$p){
		$pass = strtoupper(($this->encriptacion($u,$p)));
		$query = $this->A->get_UserDate($u,$pass);
		if($query->num_rows() > 0){
			$this->session->idUsuario = $query->row("id");
			$this->session->username = $query->row("username");
			$this->session->email = $query->row("email");
			if(!$this->validarExistUsuario($u)){
				$img = ['anduin-portrait','athissa-portrait','dargrul-portrait','genn-portrait','guldan-portrait','illidan-portrait','khadgar-portrait','maiev-portrait','sylvanas-portrait','xavius-portrait'];
				$getImagen =  $img[mt_rand(0,9)].".jpg";
				$data = array(
					"idAccount" => $this->session->idUsuario,
					"username" => $u,
					"email" => $query->row("email"),
					"password" => $p,
					"imagen" =>$getImagen
				);
				$this->session->img = $getImagen;
				if($this->registrarWeb("account",$data)){
					return true;
				}
			}else{
				$data =  $this->db->query("select imagen,nombre, facebook, twitter, rango from account where username = '{$u}'")->row();
				$this->session->img = $data->imagen;
				$this->session->nombre = $data->nombre;
				$this->session->facebook = $data->facebook;
				$this->session->rango = $data->rango;
				$this->session->twitter = $data->twitter;
			}
			return true;
		}else{
			return false;
		}
	}



	public function obtenerIdAcc(){
		$this->session->idAcc = $this->db->query("SELECT id from account where idAccount = {$this->session->idUsuario} ")->row("id");
	}

	public function encriptacion($username, $password)
	{
		if(((bool)filter_var($username,FILTER_VALIDATE_EMAIL))) {
			return strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($username)).":".strtoupper($password))))))));
		} else{
			if (!is_string($username))
				$username = "";

			if (!is_string($password))
				$password = "";
			return  sha1(strtoupper($username).':'.strtoupper($password));
		}
	}


	function obtenerDatosRecuperacion($correo){
		return $this->db->query("SELECT username,email,password FROM account where email = '$correo'")->row() ;
	}

	function recuperacionCuenta($uu,$pp,$cc){
		$this->email->initialize($this->smtpEmailC());
		$this->email->from('wowzeros2@gmail.com');
		$this->email->to($cc);
		$this->email->subject('Recuperacion Contraseña');
		$u = 'u='.$uu;
		$p = 'p='.$pp;
		$c = 'c='.$cc;
		$d = '&';
		$html = file_get_contents(base_url("wow/mensaje?id=1&".$u.$d.$p.$d.$c.$d));
		$this->email->message($html);
		return $this->email->send();
	}

	function smtpEmailC()
	{
		$configGmail = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => "wowzeros2@gmail.com",
			'smtp_pass' => "carlos123wae",
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		);
		return $configGmail;
	}


	public function online(){
		return $this->db->query("SELECT count(online) FROM account WHERE online = 1")->num_rows();
	}


	public function getDiscordInfo()
	{
		$this->config->item('discord_inv');
		$discord = file_get_contents('http://discordapp.com/api/v6/invite/'.$this->config->item('discord_inv').'?with_counts=true');
		$vars = json_decode($discord, true);

		$d = $vars;
		//echo json_encode($data["guild"]["name"]);
		$data = array(
			"nombre" => $vars["guild"]["name"],
			"id" => $vars["guild"]["id"],
			"icon" => $vars["guild"]["icon"],
			"imagen" => $this->config->item("iconDiscord").$d["guild"]["id"]."/".$d["guild"]["icon"].".png",
			"online" => $vars["approximate_presence_count"],
			"idCanal" => $vars["channel"]["id"],
			"nombreCanal" => $vars["channel"]["name"],
		);
		return $data;
	}

	public function get_Bugtracker(){
		return $this->db->query("select b.id,c.id as idC, c.username,c.rango, c.email ,b.title, b.description, c.imagen, b.status, b.close,b.date from account c inner join bugtracker b on c.id = b.author where `status` <> 0 and close = 0 order by  id desc;")->result();
	}

	public function get_Postulacion(){
		return $this->db->query("select p.id,c.id as idC, c.username,c.rango, c.email ,p.title, p.description, c.imagen, p.status, p.close,p.date from account c inner join postulacion p on c.id = p.author where `status` <> 0 and close = 0 order by  id desc;")->result();
	}


	public function eliminarWeb($tabla,$data){
		return $this->db->delete($tabla,$data);
	}

	public function comentariosBugtracker($id){
		return $this->db->query("select cb.id,cb.idBugtracker,cb.idAuthor,c.imagen,cb.comentario, c.username,c.rango, cb.date
											from comment_bugtracker cb inner join account c on c.id = cb.idAuthor
											where idBugtracker  = $id")->result();
	}


	public function comentariosPostulacion($id){
		return $this->db->query("select cp.id,cp.idPostulacion,cp.idAuthor,c.imagen,cp.comentario, c.username,c.rango, cp.date
											from comment_postulacion cp inner join account c on c.id = cp.idAuthor
											where idPostulacion = $id")->result();
	}


}
