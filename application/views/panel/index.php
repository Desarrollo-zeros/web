<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/gif" sizes="16x16" href="https://www.queltalar.online/woltk/images/favicon.gif">
	<title><?=$this->config->item("nombreServidor")?></title>
	<!-- Bootstrap Core CSS -->
	<link href="<?=base_url()?>/assets/panel/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Menu CSS -->
	<link href="<?=base_url()?>/assets/panel/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
	<!-- toast CSS -->
	<link href="<?=base_url()?>/assets/panel/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	<!-- morris CSS -->
	<link href="<?=base_url()?>/assets/panel/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
	<!-- chartist CSS -->
	<link href="<?=base_url()?>/assets/panel/plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
	<link href="<?=base_url()?>/assets/panel/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
	<!-- animation CSS -->
	<link href="<?=base_url()?>/assets/panel/css/animate.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="<?=base_url()?>/assets/panel/css/style.css" rel="stylesheet">
	<!-- color CSS -->
	<link href="<?=base_url()?>/assets/panel/css/colors/default.css" id="theme" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url()?>/assets/auto/easy-autocomplete.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="<?=base_url()?>/assets/panel/css/file.css">




	<style>





		strong {
			font-weight: bold;
		}



		body {
			text-align: center;
		}

		section {
			width: 81%;
			margin: auto;
			text-align: left;
		}
		label{
			text-align: left!important;
		}

		zoom {
			zoom: 95%;
		}

		.zooom1{
			zoom: 98%;
		}

		.zooom2{
			zoom: 95%;
		}


	</style>
	<!-- include summernote css/js -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
	<!-- ============================================================== -->
	<!-- Topbar header - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<nav class="navbar navbar-default navbar-static-top m-b-0">
		<div class="navbar-header">
			<div class="top-left-part">
				<!-- Logo -->
				<a class="logo" href="https://www.queltalar.online/QueltalarPanel/panel">
					<!-- Logo icon image, you can use font-icon also --><b>
						<!--This is dark logo icon--><img width="100" src="https://www.queltalar.online/woltk/styles/thecoldlord//images/logo.png" alt="home" class="dark-logo img-responsive" style="position: relative; left: 33px;bottom: 10px;"> <!--This is light logo icon--><img width="100" src="https://www.queltalar.online/woltk/styles/thecoldlord//images/logo.png" alt="home" class="light-logo img-responsive" style="position: relative; left: 33px;bottom: 10px;" />
					</b>
					<!-- Logo text image you can use text also --><span class="hidden-xs">

						</span>
				</a>
			</div>
			<!-- /Logo -->
			<ul class="nav navbar-top-links navbar-right pull-right">
				<li>
					<form role="search" class="app-search hidden-sm hidden-xs m-r-10" id="buscarEnPagina">
						<input type="text" placeholder="Buscar en pagina" class="form-control"> <a href=""><i class="fa fa-search"></i></a>
					</form>
				</li>
				<li>
					<a class="profile-pic" href="#"> <img id="img1x" src="<?=base_url()?>/assets/panel/perfil/<?=$perfilFoto?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?=$usuario?></b></a>
				</li>
			</ul>
		</div>

	</nav>

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav slimscrollsidebar">
			<div class="sidebar-head">
				<h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
			</div>
			<ul class="nav" id="side-menu" style="text-align: left!important;">
				<li style="padding: 70px 0 0;">
					<a href="" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Dashboard</a>
				</li>

				<li>
					<a href="#informacion" onclick="$('#rangoGm').css('display','none');loaderBugtrackers();$('#bugtracker').css('display','none');$('#informacion').css('display','block');$('#account').css('display','none');$('#postular').css('display','none');" class="waves-effect"><i class="fa fa-info fa-fw" aria-hidden="true"></i>Informacion Del Servidor</a>
				</li>

				<li>
					<a href="#account" onclick="$('#rangoGm').css('display','none');$('#bugtracker').css('display','none');$('#informacion').css('display','none');$('#account').css('display','block');$('#postular').css('display','none');" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Administra Tu Cuenta</a>
				</li>
				<li>
					<a href="#bugtracker" onclick="$('#rangoGm').css('display','none');$('#bugtracker').css('display','block');$('#informacion').css('display','none');$('#account').css('display','none');$('#postular').css('display','none');loaderBugtrackers();" class="waves-effect"><i class="fa fa-bug fa-fw" aria-hidden="true"></i>Bugtracker </a>
				</li>

				<li>
					<a href="#D-V" class="waves-effect"><i class="fa fa-buysellads fa-fw" aria-hidden="true"></i>Donacion Y Votacion</a>
				</li>


				<?php
					if(isset($this->session->rango)){
						if($this->session->rango>=6){
							echo '<li><a href="#rangoGm" id="btnrangoGm" class="waves-effect"><i class="fa fa-arrow-right fa-fw" aria-hidden="true"></i>Rango GM</a></li>';
						}else{
							echo '<li><a href="#postulacion" id="btnPostulacion" class="waves-effect"><i class="fa fa-adjust fa-fw" aria-hidden="true"></i>Postular GM</a></li>';
						}
					}
				?>

				<li>
					<a href="https://www.queltalar.online/woltk/" class="waves-effect"><i class="fa fa-internet-explorer fa-fw" aria-hidden="true"></i>Ir QuelTalar</a>
				</li>

				<li>
					<a href="<?=base_url("panel/salir")?>" class="waves-effect"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i>Salir</a>
				</li>
			</ul>
			<div class="center p-20">
			</div>
		</div>

	</div>
	<!-- ============================================================== -->
	<!-- End Left Sidebar -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Page Content -->
	<!-- ============================================================== -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row bg-title">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					<h4 class="page-title">Dashboard</h4> </div>
				<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					<ol class="breadcrumb">
						<li><a href="#">Dashboard</a></li>
					</ol>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<!-- ============================================================== -->
			<!-- Different data widgets -->
			<!-- ============================================================== -->
			<!-- .row -->
			<div class="row">
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<div class="white-box analytics-info">
						<h3 class="box-title">Online Servidor</h3>
						<ul class="list-inline two-part">
							<li>
								<div id="sparklinedash"></div>
							</li>
							<li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success"><?=$onlinePlayers?></span></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<div class="white-box analytics-info">
						<h3 class="box-title">online <?=$this->config->item("nombreServidor");?></h3>
						<ul class="list-inline two-part">
							<li>
								<div id="sparklinedash2"></div>
							</li>
							<li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple"><?=$onlineWeb?></span></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 col-xs-12">
					<div class="white-box analytics-info">
						<h3 class="box-title">Discord Online &nbsp;&nbsp;<a href="<?=$this->config->item("LinkIvinteDiscord")?>" target="_blank" title="<?=$discord["nombre"]?>"><img src="<?=$discord["imagen"];?>" width="20" style="border-radius: 40%"/></a> </h3>
						<ul class="list-inline two-part">
							<li style="height: 16px; "><a style="position: relative;bottom: 5px;" href="<?=$this->config->item("LinkIvinteDiscord")?>" target="_blank"><span style="font-size: 13px;" title="<?=$discord["nombre"];?>"><?=$discord["nombre"];?></span></a></li>
							<li class="text-right"><i class="ti-arrow-up text-info"></i><span class="counter text-info"><?=$discord["online"];?></span></li>
						</ul>
					</div>
				</div>
			</div>

			<!-- ============================================================== -->
			<!-- chat-listing & recent comments -->
			<!-- ============================================================== -->

			<div class="row" id="informacion">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="white-box">
						<h3 class="box-title">Informacion Del Servidor</h3>
						<!--<ul class="list-inline text-right">
                            <li>
                                <h5><i class="fa fa-circle m-r-5 text-info"></i>Posteado por</h5> </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5 text-inverse"></i>zeros</h5> </li>
                        </ul>-->
						<div id="noticias">

						</div>
						<div id="ct-visits"  style="display: none;"></div>
					</div>
				</div>
			</div>

			<div class="row" id="account" style="display: none;">
				<div class="col-md-4 col-xs-12">
					<div class="white-box">
						<div class="user-bg"> <img id="imgPerfil1"  width="100%" alt="user" src="<?=base_url()?>/assets/panel/perfil/<?=$perfilFoto?>">
							<div class="overlay-box">
								<div class="user-content">

									<form method="post" id="formPerfilFoto" action="" enctype="multipart/form-data" id="uploadForm" style="display: none">
										<input type="file" name="perfilFoto" id="perfilFoto" class="jfilestyle" data-input="false">
									</form>

									<a ondblclick="$('#btnPerfilFoto').css('display','none');$('#formPerfilFoto').css('display','block');" id="btnPerfilFoto" href="javascript:void(0)"><img id="imgPerfil2" src="<?=base_url()?>/assets/panel/perfil/<?=$perfilFoto?>" class="thumb-lg img-circle" alt="img"></a>
									<h4 class="text-white"><?=$usuario?></h4>
									<h5 class="text-white"><?=$correo?></h5> </div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-md-8 col-xs-12">
					<div class="white-box">
						<form id="formInformacionCuenta" class="form-horizontal form-material">
							<div class="form-group">
								<input value="<?=$this->session->idAcc?>" id="idAcc" type="hidden">
								<label class="col-md-12">Nombre Completo</label>
								<div class="col-md-12">
									<input type="text" placeholder="Nombre Completo" name="nombreCompleto" id="nombreCompleto" class="form-control form-control-line" value="<?=$nombreCompleto?>" required="required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Email</label>
								<div class="col-md-12">
									<input type="email" disabled="disabled" placeholder="<?=$correo?>" class="form-control form-control-line" name="email" id="email" value="<?=$correo?>" required="required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Contraseña</label>
								<div class="col-md-12">
									<input type="password" id="contraseña1" name="contraseña1" value="" placeholder="Contraseña" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-12">Repita su Contraseña</label>
								<div class="col-md-12">
									<input type="password" id="contraseña2" name="contraseña2" value="" placeholder="Repita la Contraseña" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-12">Telefono</label>
								<div class="col-md-12">
									<input type="text" id="telefono" name="telefono" placeholder="+573043651232" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-12">Facebook</label>
								<div class="col-md-12">
									<input type="text" id="facebook" name="facebook" placeholder="Link perfil facebook" class="form-control form-control-line">
								</div>
							</div>


							<div class="form-group">
								<label class="col-md-12">Twitter</label>
								<div class="col-md-12">
									<input type="text" id="twitter" name="twitter" placeholder="Link perfil twitter" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12" align="left">
									<input type="submit" class="btn btn-success align-left" value="Actualizar Datos"/>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>



			<div class="row" id="rangoGm" style="display: none;">
				<div class="col-md-12 col-xs-12">
					<div class="white-box">
						<form id="formDarRango" class="form-horizontal form-material">
							<div class="form-group">
								<label class="col-md-12">ID Cuenta</label>
								<div class="col-md-12">
									<input type="number" disabled="disabled" id="idCuentaGm" name="idCuentaGm" placeholder="Id Cuenta de Gm" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-12">Cuenta</label>
								<div class="col-md-12">
									<input type="search" id="cuentaGM" name="cuentaGM" placeholder="Nombre Cuenta Gm" class="form-control form-control-line">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-12">Email</label>
								<div class="col-md-12">
									<input type="email"   disabled="disabled" id="emailGM" name="emailGM" placeholder="Correo electronico del jugador" class="form-control form-control-line">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Rango</label>
								<div class="col-md-12">
									<select id="rango" class="form-control form-control-line" required="required">
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12" align="left">
									<input type="submit" class="btn btn-success align-left" value="Actualizar Datos"/>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>



			<div class="row" id="postular" style="display: none;">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="white-box">
						<h3 class="box-title">Postular Como Gm <a onclick="$('#modalPostularGm').modal('show');$('.modal-backdrop').css('display','none');btn2();" class="btn btn-info btn-sm" style="position: absolute; right: 5%">Nuevo Reporte</a></h3>
						<div class="modal fade" id="modalPostularGm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content" style="width:140%!important;left: -60px!important;">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Postulacion GM</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form id="formPostulacionGm">
											<div class="form-group">
												<label for="tituloPostulacion" class="col-form-label">Titulo:</label>
												<input type="text" class="form-control" id="tituloPostulacion" required="required">
											</div>
											<div class="form-group col-md-12">
												<p><label class="col-form-label">¿Porque debo ser gm?:</label></p>
												<div class="edit" id="edit2">
													Deja tu Comentario
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" onclick="$('#modalPostularGm').modal('toggle');" class="btn btn-info">Volver a Postulacion</button>
												<input type="submit" id="btnGudarP" class="btn btn-primary" value="Guardar Postulacion">
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="comment-center p-t-10" id="loaderPostulacion">
						</div>
					</div>
				</div>
			</div>




			<div id="bugtracker" class="row" style="display: none;">
				<!-- .col -->
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="white-box">
						<h3 class="box-title">Bugtracker / Postear Errores <a onclick="$('#modalReportar').modal('show');$('.modal-backdrop').css('display','none');btn1();" class="btn btn-info btn-sm" style="position: absolute; right: 5%">Nuevo Reporte</a></h3>
						<div class="modal fade" id="modalReportar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content" style="width:140%!important;left: -60px!important;">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">Bugtracker</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form id="formReporte">
											<div class="form-group">
												<label for="tituloReport" class="col-form-label">Titulo:</label>
												<input type="text" class="form-control" id="tituloReport" required="required">
											</div>
											<div class="form-group col-md-12">
												<p><label class="col-form-label">Detalle</label></p>
												<div class="edit" id="edit1">
													Deja tu Comentario
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" onclick="$('#modalReportar').modal('toggle');loaderBugtrackers();" class="btn btn-info">Volver a Reporte</button>
												<input type="submit" id="btnGudarR" class="btn btn-primary" value="Guardar Reporte">
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="comment-center p-t-10" id="loaderBugtracker">
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="panel">
						<div class="sk-chat-widgets">
							<div class="panel panel-default">
								<div class="panel-heading">
									CHAT LISTING -- Mantenimiento
								</div>
								<div class="panel-body">
									<ul class="chatonline">
										<li>
											<div class="call-chat">
												<button class="btn btn-success btn-circle btn-lg" type="button"><i class="fa fa-phone"></i></button>
												<button class="btn btn-info btn-circle btn-lg" type="button"><i class="fa fa-comments-o"></i></button>
											</div>
											<a href="javascript:void(0)"><img src="<?=base_url()?>/assets//panel//plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.col -->
			</div>

		</div>
		<!-- /.container-fluid -->
		<footer class="footer text-center"> 2018 &copy; stormMane </footer>
	</div>
	<!-- ============================================================== -->
	<!-- End Page Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="<?=base_url()?>/assets/panel/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?=base_url()?>/assets//panel/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=base_url()?>/assets//panel//plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?=base_url()?>/assets//panel/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url()?>/assets//panel/js/waves.js"></script>
<!--Counter js -->
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!-- chartist chart -->
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=base_url()?>/assets//panel/js/custom.min.js"></script>
<script src="<?=base_url()?>/assets//panel/js/dashboard1.js"></script>
<script src="<?=base_url()?>/assets//panel/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?=base_url()?>/assets/auto/jquery.easy-autocomplete.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

<script src="<?=base_url()?>/assets/notify.js"></script>
<script src="<?=base_url()?>/assets/panel/js/file.js"></script>


<div id="myModal" class="modal">
	<img class="modal-content" id="img01">
	<div id="caption"></div>
</div>




<div id="itModal" style="display: none;">
	<span id="btnModal2"></span>
	<img id="imgModal" class="img-rounded" width="25"  src="<?=base_url()?>/assets/custom/quest_start.gif"/>
</div>

</body>


<script>



	var url = '<?=base_url()?>';
	var dataC = [];
	var idAcc = <?=$this->session->idAcc?>;
	$rango = "";

	$(document).ready(function () {
		getRangos();
	})

	$("#buscarEnPagina").on("submit",function (form) {
		form.preventDefault();
	});

	$(document).ready(function () {
		loaderBugtrackers();
		loaderPostulacion();
	});

	/*no tocar*/
	function loaderBugtrackers() {
		$.ajax({
			url: url+"panel/bugtracker", dataType : 'JSON', type : 'POST',
			success : function (data) {
				var string = '';
				for(var i in data){
					var username = rangos(data[i].rango,MaysPrimera(data[i].username));
					console.log(username);
					string += '<div class="comment-body">';
					string += '<div class="user-img"> ';
					string += '<img src="<?=base_url()?>/assets/panel/perfil/'+data[i].imagen+'" alt="user" class="img-circle">';
					string += '</div>';
					string += '<div align="left" class="mail-contnet">';
					string += '<input id="idAcc-reporte-'+data[i].id+'" value="'+data[i].idC+'" type="hidden">';
					string += '<h5>'+username+'<a onclick="btnDelReporte('+data[i].id+')" style="left: 90%!important;position: absolute;" class="fa fa-close text-danger"></a><a onclick="btnMComentarioReporte('+data[i].id+');cargarComentariosReporte('+data[i].id+');" style="left: 88%!important;position: absolute;" class="fa fa-minus text-info"></a></h5><span class="time">Fecha Publicacion: '+timeStapToDate(data[i].date)+'</span>';
					string += '<h5>Titulo: '+data[i].title+'</h5><h5>Estado: '+estado(data[i].status)+'</h5>';
					string += '<div id="id-Reporte-'+data[i].id+'" style="display:none">';
					string += '<div>'+data[i].description+'</div>'
					string += '<br><div>'+comentariosReporte(data[i].id)+'</div>';
					string += '<div id="cargarComentariosReporte-id-'+data[i].id+'"></div>';
					string += '<br></div></div>';
					string += '</div>';
				}
				$("#loaderBugtracker").html(string);
			}
		});
	}

	function rangos(rango,nombre){
		getRangos();
		rango = (rango < 1) ? 0 : rango;
		rango = $rango[rango].rango_nombre+": "+nombre;
		var url = '<a style="color: #365899;cursor: pointer;text-decoration:none;">'+rango+'</a>';
		return url;
	}

	function getRangos() {
		$.ajax({
			url: url+"panel/getRangos", dataType : 'JSON', type : 'POST',
			success : function (data) {
				$rango = data;
			}
		});
	}


	function loaderPostulacion() {
		$.ajax({
			url: url+"panel/postulacion", dataType : 'JSON', type : 'POST',
			success : function (data) {
				var string = '';
				for(var i in data){
					var username = rangos(data[i].rango,MaysPrimera(data[i].username));
					console.log(username);
					string += '<div class="comment-body">';
					string += '<div class="user-img"> ';
					string += '<img src="<?=base_url()?>/assets/panel/perfil/'+data[i].imagen+'" alt="user" class="img-circle">';
					string += '</div>';
					string += '<div align="left" class="mail-contnet">';
					string += '<input id="idAcc-postulacion-'+data[i].id+'" value="'+data[i].idC+'" type="hidden">';
					string += '<h5>'+username+'<a onclick="btnDelPostulacion('+data[i].id+')" style="left: 90%!important;position: absolute;" class="fa fa-close text-danger"></a><a onclick="btnMComentarioPostulacion('+data[i].id+');cargarComentariosPostulacion('+data[i].id+');" style="left: 88%!important;position: absolute;" class="fa fa-minus text-info"></a></h5><span class="time">Fecha Publicacion: '+timeStapToDate(data[i].date)+'</span>';
					string += '<h5>Titulo: '+data[i].title+'</h5><h5>Estado: '+estado(data[i].status)+'</h5>';
					string += '<div id="id-Postulacion-'+data[i].id+'" style="display:none">';
					string += '<div>'+data[i].description+'</div>'
					string += '<div>'+comentariosPostulacion(data[i].id)+'</div>';
					string += '<div id="cargarComentariosPostulacion-id-'+data[i].id+'"></div>';
					string += '<br></div></div>';
					string += '</div>';
				}
				$("#loaderPostulacion").html(string);
			}
		});
	}

	function comentariosReporte(id){
		var string = '';
		string += '<form id="id-comentario-report-'+id+'">';
		string += '<div class="form-group col-md-12 ">';
		string += '<div contenteditable="true" id="txt-reporte-'+id+'" spellcheck="false" class="form-control form-rounded texarea-key"  style="width: 500px!important;height:auto!important;border-radius: 20px!important;"></div><br>';
		string += '<a href="javascript:void(0);"  onclick="guardarComentarioReporte('+id+')" class="btn-rounded btn btn-default btn-outline">';
		string += '<i class="ti-check text-success m-r-5"></i>Comentar</a>';
		string += '<input type="hidden" id="input-reporte-idC-'+id+'" value="'+idAcc+'" >';
		string += '</div></form>';
		return string;
	}

	function comentariosPostulacion(id){
		var string = '';
		string += '<form id="id-comentario-postulacion-'+id+'">';
		string += '<div class="form-group col-md-12 ">';
		string += '<div contenteditable="true" id="txt-postulacion-'+id+'" spellcheck="false"  class="form-control form-rounded texarea-key"  style="width: 500px!important;height:auto!important;border-radius: 20px!important;"></div><br>';
		string += '<a href="javascript:void(0);"  onclick="guardarComentarioPostulacion('+id+')" class="btn-rounded btn btn-default btn-outline">';
		string += '<i class="ti-check text-success m-r-5"></i>Comentar</a>';
		string += '<input type="hidden" id="input-postulacion-idC-'+id+'" value="'+idAcc+'" >';
		string += '</div></form>';
		return string;
	}

	function replaceURLWithHTMLLinks(text)
	{
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		return text.replace(exp,"<a href='$1'target='_blank'>$1</a>");
	}

	function guardarComentarioReporte(id) {
		if(confirm("desea guardar este comentario")){
			var obj = new Object();
			obj.idB = id;
			obj.idC = $("#input-reporte-idC-"+id+"").val();
			obj.comentario = replaceURLWithHTMLLinks($("#txt-reporte-"+id+"").html());

			$.ajax({
				url: '<?=base_url("panel/guardarComentarioReporte")?>',
				dataType : 'JSON',
				type : 'POST',
				data : obj,
				success : function(data) {
					if(data){
						cargarComentariosReporte(obj.idB);
					}else{
						$.notify("Error No se pudo guardar el comentario","error");
					}

				}
			});
		}
	}

	function guardarComentarioPostulacion(id) {
		if(confirm("desea guardar este comentario")){
			var obj = new Object();
			obj.idB = id;
			obj.idC = $("#input-postulacion-idC-"+id+"").val();
			obj.comentario = replaceURLWithHTMLLinks($("#txt-postulacion-"+id+"").html());

			$.ajax({
				url: '<?=base_url("panel/guardarComentarioPostulacion")?>',
				dataType : 'JSON',
				type : 'POST',
				data : obj,
				success : function(data) {
					if(data){
						cargarComentariosPostulacion(obj.idB);
					}else{
						$.notify("Error No se pudo guardar el comentario","error");
					}

				}
			});
		}
	}

	function cargarComentariosReporte(id) {
		$.ajax({
			url: url+"panel/buscarComentariosReportes?id="+id+"",
			dataType : 'JSON',
			type : 'GET',
			success : function(data) {
				var string = '';
				for(var i in data){
					var username = rangos(data[i].rango,MaysPrimera(data[i].username));
					var username1 = MaysPrimera(data[i].username);
					string += '<div class="form-group col-md-12">';
					string +=  '<div contenteditable="false" disabled="disabled" id="div-reporte-'+id+'" spellcheck="false" class="form-control form-rounded col-md-12" style="width: 500px;height: auto;"><img src="<?=base_url()?>/assets/panel/perfil/'+data[i].imagen+'" alt="'+username1+'" title="'+username1+'" width="10" class="img-circle col-md-2"/>'+username+'  '+data[i].comentario+' <a onclick="btnComentarioReporteDel('+data[i].id+')"  style="float: right" class="fa fa-close text-danger"></a><span style="float: right" class="time">Fecha Publicacion: '+timeStapToDate(data[i].date)+'</span></div>';
					string += '<input id="input-reporte-cC-idA-'+data[i].id+'" type="hidden" value="'+data[i].idAuthor+'">';
					string += '<input id="input-reporte-re-id-'+data[i].id+'" type="hidden" value="'+id+'">';
					string += '</div>';
				}
				$("#cargarComentariosReporte-id-"+id+"").html(string);
			}
		});
	}

	function cargarComentariosPostulacion(id) {

		$.ajax({
			url: url+"panel/buscarComentarioPostulacion?id="+id+"",
			dataType : 'JSON',
			type : 'GET',
			success : function(data) {
				var string = '';
				for(var i in data){
					var username = rangos(data[i].rango,MaysPrimera(data[i].username));
					var username1 = MaysPrimera(data[i].username);
					string += '<div class="form-group col-md-12">';
					string +=  '<div contenteditable="false" disabled="disabled" id="div-postulacion-'+id+'" spellcheck="false" class="form-control form-rounded col-md-12" style="width: 500px;height: auto;"><img src="<?=base_url()?>/assets/panel/perfil/'+data[i].imagen+'" alt="'+username1+'" title="'+username1+'" width="10" class="img-circle col-md-2"/>'+username+'  '+data[i].comentario+' <a onclick="btnComentarioPostulacionDel('+data[i].id+')"  style="float: right" class="fa fa-close text-danger"></a><span style="float: right" class="time">Fecha Publicacion: '+timeStapToDate(data[i].date)+'</span></div>';
					string += '<input id="input-postulacion-cC-idA-'+data[i].id+'" type="hidden" value="'+data[i].idAuthor+'">';
					string += '<input id="input-postulacion-re-id-'+data[i].id+'" type="hidden" value="'+id+'">';
					string += '</div>';
				}
				$("#cargarComentariosPostulacion-id-"+id+"").html(string);
			}
		});
	}

	function MaysPrimera(string){
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	}

	function btnDelReporte(id) {
		console.log("data");
		if(idAcc == $("#idAcc-reporte-"+id+"").val()){

			if(confirm("desea eliminar este reporte")){
				$.ajax({
					url: '<?=base_url("panel/eliminarReporte")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						id : id,
						idAcc : idAcc
					},
					success : function(data) {
						if(data){
							$.notify("Se ha eliminado el reporte correctamente","success");
							loaderBugtrackers();
						}else{
							$.notify("Error No se pudo eliminar el reporte","error");
						}

					}
				});
			}
		}else{
			$.notify("no tienes permiso para eliminar este reporte");
		}
	}

	function btnDelPostulacion(id) {
		if(idAcc == $("#idAcc-postulacion-"+id+"").val()){

			if(confirm("desea eliminar esta Postulacion")){
				$.ajax({
					url: '<?=base_url("panel/eliminarPostulacion")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						id : id,
						idAcc : idAcc
					},
					success : function(data) {
						if(data){
							$.notify("Se ha eliminado el reporte correctamente","success");
							loaderPostulacion();
						}else{
							$.notify("Error No se pudo eliminar la postulacion","error");
						}

					}
				});
			}
		}else{
			$.notify("no tienes permiso para eliminar este reporte");
		}
	}

	function btnComentarioReporteDel(id) {
		if(idAcc == $("#input-reporte-cC-idA-"+id+"").val()){
			cargarComentariosReporte(id);
			if(confirm("desea eliminar este comentario?")){
				$.ajax({
					url: '<?=base_url("panel/eliminarComentarioReporte")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						id : id,
						idAcc: idAcc
					},
					success : function(data) {
						if(data){
							cargarComentariosReporte($("#input-reporte-re-id-"+id+"").val())
							$.notify("Se ha eliminado el comentario correctamente","success");
						}else{
							$.notify("Error No se pudo eliminar el comentario","error");
						}
					}
				});
			}
		}else{
			$.notify("no tienes permiso para eliminar este comentario");
		}
		console.log("cargando");
	}

	function btnMComentarioReporte(id) {
		if($("#id-Reporte-"+id+"").hasClass('abierto')){
			$("#id-Reporte-"+id+"").css("display","none");
			$("#id-Reporte-"+id+"").removeClass('abierto');
			cargarComentariosReporte(id);
		}else{
			$("#id-Reporte-"+id+"").css("display","block");
			$("#id-Reporte-"+id+"").addClass('abierto');
		}
	}

	function btnMComentarioPostulacion(id) {
		if($("#id-Postulacion-"+id+"").hasClass('abierto')){
			$("#id-Postulacion-"+id+"").css("display","none");
			$("#id-Postulacion-"+id+"").removeClass('abierto');
			cargarComentariosPostulacion(id);
		}else{
			$("#id-Postulacion-"+id+"").css("display","block");
			$("#id-Postulacion-"+id+"").addClass('abierto');
		}
	}

	function btnComentarioPostulacionDel(id) {
		if(idAcc == $("#input-postulacion-cC-idA-"+id+"").val()){
			cargarComentariosReporte(id);
			if(confirm("desea eliminar este comentario?")){
				$.ajax({
					url: '<?=base_url("panel/eliminarComentarioPostulacion")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						id : id,
						idAcc:idAcc
					},
					success : function(data) {
						if(data){
							cargarComentariosPostulacion($("#input-postulacion-re-id-"+id+"").val())
							$.notify("Se ha eliminado el comentario correctamente","success");
						}else{
							$.notify("Error No se pudo eliminar el comentario","error");
						}
					}
				});
			}
		}else{
			$.notify("no tienes permiso para eliminar este comentario");
		}
		console.log("cargando");
	}




	$("#formReporte").on("submit",function (form) {
		form.preventDefault();
		if($('#edit1').summernote('code') != ""){
			if(confirm("desea guardar este reporte?")){
				$("input").prop('disabled', true);
				$.notify("Por favor, espera mientra se guarda tu reporte","info");
				$.ajax({
					url: '<?=base_url("panel/guardarReporte")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						titulo : $("#tituloReport").val(),
						html : $('#edit1').summernote('code')
					},
					success : function(data) {
						if(data){
							$.notify("Se ha guardado tu reporte correctamente, gracias por colocaborar con la comunidad","success");
							loaderBugtrackers();
						}else{
							$.notify("Error No se pudo guardar el reporte","error");
						}
						$("input").prop('disabled', false);
					}
				});
			}
		}

	});


	$("#formPostulacionGm").on("submit",function (form) {
		form.preventDefault();
		if($('#edit2').summernote('code') != ""){
			if(confirm("desea guardar este comentario?")) {
				$("input").prop('disabled', true);
				$.notify("Por favor, espera mientra se guarda tu postulacion","info");
				$.ajax({
					url: '<?=base_url("panel/guardarPostulacion")?>',
					dataType : 'JSON',
					type : 'POST',
					data : {
						titulo : $("#tituloPostulacion").val(),
						html : $('#edit2').summernote('code')
					},
					success : function(data) {
						if(data){
							$.notify("Se ha guardado tu reporte correctamente, gracias por colocaborar con la comunidad","success");
							loaderPostulacion();
						}else{
							$.notify("Error No se pudo guardar el reporte","error");
						}
						$("input").prop('disabled', false);
					}
				});

			}
		}
	});


	function timeStapToDate(data) {
		// Unixtimestamp
		var unixtimestamp = data;
		// Months array
		var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		// Convert timestamp to milliseconds
		var date = new Date(unixtimestamp*1000);
		// Year
		var year = date.getFullYear();
		// Month
		var month = months_arr[date.getMonth()];
		// Day
		var day = date.getDate();
		// Hours
		var hours = date.getHours();
		// Minutes
		var minutes = "0" + date.getMinutes();
		// Seconds
		var seconds = "0" + date.getSeconds();
		// Display date time in MM-dd-yyyy h:m:s format
		var convdataTime = month+'-'+day+'-'+year+' '+hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
		return convdataTime;
	}


	function estado(e) {
		$data = ["Nuevo reporte",
			"Esperando más información",
			"Informe confirmado",
			"En progreso",
			"Prueba de corrección de necesidad",
			"Reparar necesita revisión",
			"Inválido",
			"Resuelto"];
		return estadoColor($data[e],e);
	}


	function estadoColor(t,e) {
		if(e  == 1){
			return '<span style="color: #0500ff">'+t+'</span>';
		}
		if(e  == 2){
			return '<span style="color: #00c6ff">'+t+'</span>';
		}
		if(e  == 3){
			return '<span style="color: #00ff9e">'+t+'</span>';
		}
		if(e  == 4){
			return '<span style="color: #08ffa4">'+t+'</span>';
		}
		if(e  == 5){
			return '<span style="color: #00ff7f">'+t+'</span>';
		}
		if(e  == 6){
			return '<span style="color: rgba(203,255,0,0.98)">'+t+'</span>';
		}
		if(e  == 7){
			return '<span style="color: #ff1700">'+t+'</span>';
		}

		if(e  == 8){
			return '<span style="color: #1eff00">'+t+'</span>';
		}
	}
	/*no tocar*/



	function filePreview(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#imgPerfil1').attr("src",e.target.result);
				$('#imgPerfil2').attr("src",e.target.result);
				$("#img1x").attr("src",e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#perfilFoto").change(function () {
		filePreview(this);
		$('#btnPerfilFoto').css('display','block');
		$('#formPerfilFoto').css('display','none');
		var formData = new FormData();
		formData.append("file", this.files[0]);
		var $el = $("#perfilFoto");
		$el.wrap('<form>').closest('form').get(0).reset();
		$el.unwrap();
		formData.append("perfil", "perfil");
		$.ajax({
			url : url+"/panel/uploads",
			dataType:'JSON',
			type:'POST',
			data: formData,
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if(data){
					$.notify("se ha cambiado la imagen de perfil","success");
				}else{
					$.notify("uish, si tu no sabes que paso, menos yo...","error");
				}
			}
		});
	});


	$("#formInformacionCuenta").on("submit",function (form) {
		form.preventDefault();

		if(confirm("Desea Actualizar Datos Personales?")){

			var obj = new Object();
			obj.nombre = $("#nombreCompleto").val();
			obj.facebook = $("#facebook").val();
			obj.twitter = $("#twitter").val();
			obj.telefono = $("#telefono").val();
			obj.idAcc = $("#idAcc").val();
			obj.password = $("#contraseña1").val();

			if($("#contraseña1").val() == $("#contraseña2").val()){
				if(obj.password != ""){
					if(obj.password.length<5){
						$.notify("Contraseña debes tener por lo menos 5 caracteres","success");
						return;
					}
				}
				$("input").prop('disabled', true);
				$.ajax({
					url: url+"/panel/actualizarInformacionPersonal",
					dataType : 'json',
					type :'POST',
					data : obj,
					success : function (data) {
						if(data){
							$.notify("Datos personal actualizados","success");
						}else{
							$.notify("uish, si tu no sabes que paso, menos yo...","error");
						}
						$("input").prop('disabled', false);
					}
				});
			}
		}
	});


	function  btn1() {
		$('#edit1').summernote({
			placeholder: 'Deja tu Comentario\n',
			tabsize: 2,
			height: 150,                 // set editor height
		});
		$("body").addClass("zooom1");
		$("body").addClass("zooom2");
	}

	function  btn2() {
		$('#edit2').summernote({
			placeholder: 'Deja tu Comentario\n',
			tabsize: 2,
			height: 150,                 // set editor height
		});
		$("body").addClass("zooom1");
		$("body").addClass("zooom2");
	}

	$("#btnrangoGm").click(function () {
		$('#bugtracker').css('display','none');
		$('#informacion').css('display','none');
		$('#account').css('display','none');
		$('#postular').css('display','none');
		$('#rangoGm').css('display','block');
		setTimeout(function(){
			var string = "";
			var rango = '<?=$this->session->rango?>';
			for(var i in $rango){
				if($rango[i].rango < rango){
					string += "<option value='"+$rango[i].rango+"'>"+$rango[i].rango_nombre+"</option>"
				}
			}
			$("#rango").html(string);
		},1000);
	});


	$("#cuentaGM").on("keyup", function() {
		var obj = new Object();

		if($("#cuentaGM").val().length > 2) {
			obj.username = $("#cuentaGM").val();
			$.ajax({
				url: url + "/panel/getAccount",
				dataType: 'json',
				type: 'POST',
				data: obj,
				success: function (data) {
					$("#idCuentaGm").val(data.id);
					$("#cuentaGM").val(data.username);
					$("#emailGM").val(data.email);
				}
			});
		}else{
			$.notify("debes usar un usuario valido");
		}
	});


	$("#formDarRango").on("submit",function (form) {
		form.preventDefault();
		var obj = new Object();
		if($("#idCuentaGm").val().length>0 && $("#rango").val().length>0){
			obj.id  = $("#idCuentaGm").val();
			obj.gmlevel = $("#rango").val();

			$.ajax({
				url: url + "/panel/setRango",
				dataType: 'json',
				type: 'POST',
				data: obj,
				success: function (data) {
					$("#cuentaGM").val(data.username);
					$("#emailGM").val(data.email);
				}
			});

		}
	});

	$("#btnPostulacion").click(function () {
		$('#bugtracker').css('display','none');
		$('#informacion').css('display','none');
		$('#account').css('display','none');
		$('#rangoGm').css('display','none');
		$('#postular').css('display','block');
		loaderPostulacion();
	});

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>


</html>
